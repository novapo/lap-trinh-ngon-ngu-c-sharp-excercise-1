﻿namespace bt
{
    partial class Hotel
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Hotel));
            this.btn_Them = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.txt_MaPhong = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.DataGridView_HotelList = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label3 = new System.Windows.Forms.Label();
            this.txt_GiaPhong = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txt_NgayO = new System.Windows.Forms.TextBox();
            this.btn_Dat = new System.Windows.Forms.Button();
            this.btn_Huy = new System.Windows.Forms.Button();
            this.btn_TToan = new System.Windows.Forms.Button();
            this.cbo_LoaiPhong = new System.Windows.Forms.ComboBox();
            this.cbo_TTPhong = new System.Windows.Forms.ComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridView_HotelList)).BeginInit();
            this.SuspendLayout();
            // 
            // btn_Them
            // 
            this.btn_Them.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btn_Them.BackColor = System.Drawing.Color.CornflowerBlue;
            this.btn_Them.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Them.ForeColor = System.Drawing.Color.Black;
            this.btn_Them.Location = new System.Drawing.Point(591, 197);
            this.btn_Them.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btn_Them.Name = "btn_Them";
            this.btn_Them.Size = new System.Drawing.Size(236, 42);
            this.btn_Them.TabIndex = 14;
            this.btn_Them.Text = "Thêm phòng";
            this.btn_Them.UseVisualStyleBackColor = false;
            this.btn_Them.Click += new System.EventHandler(this.btn_Them_Click);
            // 
            // label4
            // 
            this.label4.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(10, 311);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(226, 31);
            this.label4.TabIndex = 5;
            this.label4.Text = "Tình trạng phòng:";
            // 
            // txt_MaPhong
            // 
            this.txt_MaPhong.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.txt_MaPhong.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_MaPhong.Location = new System.Drawing.Point(286, 191);
            this.txt_MaPhong.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txt_MaPhong.Name = "txt_MaPhong";
            this.txt_MaPhong.Size = new System.Drawing.Size(265, 38);
            this.txt_MaPhong.TabIndex = 13;
            this.txt_MaPhong.Text = "A001";
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(10, 231);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(162, 31);
            this.label2.TabIndex = 9;
            this.label2.Text = "Loại phòng :";
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(10, 191);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(148, 31);
            this.label1.TabIndex = 10;
            this.label1.Text = "Mã phòng :";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pictureBox1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox1.BackgroundImage")));
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox1.Location = new System.Drawing.Point(255, 10);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(334, 169);
            this.pictureBox1.TabIndex = 4;
            this.pictureBox1.TabStop = false;
            // 
            // DataGridView_HotelList
            // 
            this.DataGridView_HotelList.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.DataGridView_HotelList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridView_HotelList.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2,
            this.Column3,
            this.Column4,
            this.Column5});
            this.DataGridView_HotelList.Location = new System.Drawing.Point(8, 412);
            this.DataGridView_HotelList.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.DataGridView_HotelList.Name = "DataGridView_HotelList";
            this.DataGridView_HotelList.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.AutoSizeToAllHeaders;
            this.DataGridView_HotelList.RowTemplate.Height = 28;
            this.DataGridView_HotelList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DataGridView_HotelList.Size = new System.Drawing.Size(819, 222);
            this.DataGridView_HotelList.TabIndex = 15;
            this.DataGridView_HotelList.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellClick);
            // 
            // Column1
            // 
            this.Column1.HeaderText = "Mã phòng";
            this.Column1.MinimumWidth = 6;
            this.Column1.Name = "Column1";
            this.Column1.Width = 125;
            // 
            // Column2
            // 
            this.Column2.HeaderText = "Loại phòng";
            this.Column2.MinimumWidth = 6;
            this.Column2.Name = "Column2";
            this.Column2.Width = 125;
            // 
            // Column3
            // 
            this.Column3.HeaderText = "Giá phòng";
            this.Column3.MinimumWidth = 6;
            this.Column3.Name = "Column3";
            this.Column3.Width = 125;
            // 
            // Column4
            // 
            this.Column4.HeaderText = "Tình trạng phòng";
            this.Column4.MinimumWidth = 6;
            this.Column4.Name = "Column4";
            this.Column4.Width = 125;
            // 
            // Column5
            // 
            this.Column5.HeaderText = "Số ngày ở";
            this.Column5.MinimumWidth = 6;
            this.Column5.Name = "Column5";
            this.Column5.Width = 125;
            // 
            // label3
            // 
            this.label3.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(10, 271);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(153, 31);
            this.label3.TabIndex = 9;
            this.label3.Text = "Giá phòng :";
            // 
            // txt_GiaPhong
            // 
            this.txt_GiaPhong.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.txt_GiaPhong.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_GiaPhong.Location = new System.Drawing.Point(286, 271);
            this.txt_GiaPhong.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txt_GiaPhong.Name = "txt_GiaPhong";
            this.txt_GiaPhong.Size = new System.Drawing.Size(265, 38);
            this.txt_GiaPhong.TabIndex = 12;
            this.txt_GiaPhong.Text = "150000";
            // 
            // label5
            // 
            this.label5.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(9, 351);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(150, 31);
            this.label5.TabIndex = 5;
            this.label5.Text = "Số ngày ở :";
            // 
            // txt_NgayO
            // 
            this.txt_NgayO.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.txt_NgayO.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_NgayO.Location = new System.Drawing.Point(285, 351);
            this.txt_NgayO.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txt_NgayO.Name = "txt_NgayO";
            this.txt_NgayO.Size = new System.Drawing.Size(265, 38);
            this.txt_NgayO.TabIndex = 11;
            this.txt_NgayO.Text = "0";
            // 
            // btn_Dat
            // 
            this.btn_Dat.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btn_Dat.BackColor = System.Drawing.Color.CornflowerBlue;
            this.btn_Dat.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Dat.ForeColor = System.Drawing.Color.Black;
            this.btn_Dat.Location = new System.Drawing.Point(591, 244);
            this.btn_Dat.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btn_Dat.Name = "btn_Dat";
            this.btn_Dat.Size = new System.Drawing.Size(236, 42);
            this.btn_Dat.TabIndex = 14;
            this.btn_Dat.Text = "Đặt phòng";
            this.btn_Dat.UseVisualStyleBackColor = false;
            this.btn_Dat.Click += new System.EventHandler(this.btn_Dat_Click);
            // 
            // btn_Huy
            // 
            this.btn_Huy.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btn_Huy.BackColor = System.Drawing.Color.CornflowerBlue;
            this.btn_Huy.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Huy.ForeColor = System.Drawing.Color.Black;
            this.btn_Huy.Location = new System.Drawing.Point(591, 291);
            this.btn_Huy.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btn_Huy.Name = "btn_Huy";
            this.btn_Huy.Size = new System.Drawing.Size(236, 42);
            this.btn_Huy.TabIndex = 14;
            this.btn_Huy.Text = "Hủy phòng";
            this.btn_Huy.UseVisualStyleBackColor = false;
            this.btn_Huy.Click += new System.EventHandler(this.btn_Huy_Click);
            // 
            // btn_TToan
            // 
            this.btn_TToan.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btn_TToan.BackColor = System.Drawing.Color.CornflowerBlue;
            this.btn_TToan.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_TToan.ForeColor = System.Drawing.Color.Black;
            this.btn_TToan.Location = new System.Drawing.Point(591, 338);
            this.btn_TToan.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btn_TToan.Name = "btn_TToan";
            this.btn_TToan.Size = new System.Drawing.Size(236, 42);
            this.btn_TToan.TabIndex = 14;
            this.btn_TToan.Text = "Thanh toán";
            this.btn_TToan.UseVisualStyleBackColor = false;
            this.btn_TToan.Click += new System.EventHandler(this.btn_TToan_Click);
            // 
            // cbo_LoaiPhong
            // 
            this.cbo_LoaiPhong.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbo_LoaiPhong.FormattingEnabled = true;
            this.cbo_LoaiPhong.Items.AddRange(new object[] {
            "A",
            "B",
            "C"});
            this.cbo_LoaiPhong.Location = new System.Drawing.Point(286, 229);
            this.cbo_LoaiPhong.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cbo_LoaiPhong.Name = "cbo_LoaiPhong";
            this.cbo_LoaiPhong.Size = new System.Drawing.Size(264, 39);
            this.cbo_LoaiPhong.TabIndex = 16;
            this.cbo_LoaiPhong.Text = "A";
            // 
            // cbo_TTPhong
            // 
            this.cbo_TTPhong.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbo_TTPhong.FormattingEnabled = true;
            this.cbo_TTPhong.Items.AddRange(new object[] {
            "Trống",
            "Không trống"});
            this.cbo_TTPhong.Location = new System.Drawing.Point(286, 310);
            this.cbo_TTPhong.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cbo_TTPhong.Name = "cbo_TTPhong";
            this.cbo_TTPhong.Size = new System.Drawing.Size(264, 39);
            this.cbo_TTPhong.TabIndex = 16;
            this.cbo_TTPhong.Text = "Trống";
            // 
            // Hotel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(839, 683);
            this.Controls.Add(this.cbo_TTPhong);
            this.Controls.Add(this.cbo_LoaiPhong);
            this.Controls.Add(this.DataGridView_HotelList);
            this.Controls.Add(this.btn_TToan);
            this.Controls.Add(this.btn_Huy);
            this.Controls.Add(this.btn_Dat);
            this.Controls.Add(this.btn_Them);
            this.Controls.Add(this.txt_NgayO);
            this.Controls.Add(this.txt_GiaPhong);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txt_MaPhong);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pictureBox1);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Hotel";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = " 4";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridView_HotelList)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btn_Them;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txt_MaPhong;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.DataGridView DataGridView_HotelList;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txt_GiaPhong;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txt_NgayO;
        private System.Windows.Forms.Button btn_Dat;
        private System.Windows.Forms.Button btn_Huy;
        private System.Windows.Forms.Button btn_TToan;
        private System.Windows.Forms.ComboBox cbo_LoaiPhong;
        private System.Windows.Forms.ComboBox cbo_TTPhong;
    }
}